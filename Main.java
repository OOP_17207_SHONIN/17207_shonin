package Dimas_prosto_class;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws Exception {

        try (Stream<String> stream = Files.lines(Paths.get("src/Resource/1.txt"));
             FileWriter writer = new FileWriter("src/Resource/2.csv", false)) {
             long[] word_count = new long[1];
             Map<String, Long> hashMap = stream
                    .map(word -> word.split("[^\\pN\\pL]+"))
                    .flatMap(Arrays::stream)
                    .filter(str -> 0 != str.length())
                    .peek(word -> word_count[0]++)
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

             hashMap
                    .entrySet()
                    .stream()
                    .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
                    .forEach(e -> {
                        try {
                            writer.write(e.getKey() + ";" + e.getValue() + ";%.3f" + ((((double) e.getValue() / word_count[0])) * 100) + "\n");
                        } catch (Exception er) {
                            System.out.println(er.getMessage());
                            throw new RuntimeException(er);
                        }
                    });
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}